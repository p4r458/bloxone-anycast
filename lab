#!/bin/bash

if [ $# -eq 0 ]; then
        echo "                                                         +-----+
                                                         |     |
                                                         |     |
                               +------+                  |     |
                               |      |   172.16.2.0/24  | s2  |
                               |  r2  +------------------+     |
                               |      |                  |     |
                               +---+--+                  |     |
                                   |                     +-----+
                                   |
       +--------+                  |
       |        |                  | 10.1.1.0/24
       | bind9  |                  |
       |        |                  |                     +-----+
       +---+----+                  |                     |     |
           |                       |                     |     |
           |                   +---+--+                  |     |
+------+   |  192.168.1.0/24   |      |                  |     |
|      |   |                   |  r1  |   172.16.1.0/24  | s1  |
|  c1  +---+-------------------+      +------------------+     |
+------+                       +------+                  |     |
                                                         |     |
                                                         +-----+
"

        echo "args: up, down, clean"
elif [ $1 == "up" ]; then
        # create lab bridges
        docker network create c1-r1 --subnet=192.168.1.0/24
        docker network create r1-s1 --subnet=172.16.1.0/24
        docker network create r1-r2 --subnet=10.1.1.0/24
        docker network create r2-s2 --subnet=172.16.2.0/24
        sleep 3
        
        # create BloxOne bare-metal hosts
        docker run -d --privileged -it --name s2 --net r2-s2 supblox/dind
        docker run -d --privileged -it --name s1 --net r1-s1 supblox/dind
        sleep 3
        
        # create Vyos routers and attach lab bridges
        docker run -d -it --privileged --name r1 --hostname r1 -v /lib/modules:/lib/modules -v `pwd`/conf1:/conf1 supblox/vyos:latest /sbin/init
        docker network connect r1-r2 r1
        docker network connect r1-s1 r1
        docker network connect c1-r1 r1
        sleep 3
        docker run -d -it --privileged --name r2 --hostname r2 -v /lib/modules:/lib/modules -v `pwd`/conf2:/conf2 supblox/vyos:latest /sbin/init
        docker network connect r1-r2 r2
        docker network connect r2-s2 r2
        sleep 3
        
        # create client
        docker run -d --privileged -it --name c1 ubuntu
        docker network connect c1-r1 c1
        sleep 3
        
        # create Bind9 server
        docker run -d --privileged -it --name bind9 ubuntu
        docker network connect c1-r1 bind9
        docker ps
        
        # deploy ospf configurations on routers
        docker exec -it r1 bash -c 'chmod +x conf1; ./conf1'
        docker exec -it r2 bash -c 'chmod +x conf2; ./conf2'
        
        # install ping and dig in client
        docker exec -it c1 bash -c 'apt update; apt install -y iproute2 dnsutils iputils-ping nano'
        
        # ready the bind9 dns server
        docker exec -it bind9 bash -c 'apt update; apt-get install -y bind9 iproute2 dnsutils iputils-ping nano'
        docker cp named.conf.local bind9:/etc/bind/named.conf.local 
        docker cp anyzone.com.zone bind9:/var/cache/bind/anyzone.com.zone
        docker exec -it bind9 service bind9 start
        
        # set bind9 server's default gateway to r1
        docker exec -it bind9 bash -c 'ip route delete default; ip route add default via 192.168.1.2'
        
        # set client's default gateway to r1
        docker exec -it c1 bash -c 'ip route delete default; ip route add default via 192.168.1.2'
        
        # deplow BloxOne in the designated hosts
        docker exec -it s1 bash -c 'apt update; apt install git -y && git clone https://gitlab.com/p4r458/bloxone.git; bash bloxone/setup'
        docker exec -it s2 bash -c 'apt update; apt install git -y && git clone https://gitlab.com/p4r458/bloxone.git; bash bloxone/setup'
        docker exec -it s1 bash -c 'ip route add 192.168.1.0/24 via 172.16.1.3'
        docker exec -it s2 bash -c 'ip route add 192.168.1.0/24 via 172.16.2.3'
        docker exec -it s1 bash -c 'ip route add 10.1.1.0/24 via 172.16.1.3'
        docker exec -it s2 bash -c 'ip route add 10.1.1.0/24 via 172.16.2.3'
        docker exec -it s1 bash -c 'ip route add 172.16.2.0/24 via 172.16.1.3'
        docker exec -it s2 bash -c 'ip route add 172.16.1.0/24 via 172.16.2.3'
        
elif [ $1 == "down" ]; then
        docker rm $(docker kill c1 r1 r2 s1 s2 bind9)
elif [ $1 == "clean" ]; then
        docker rm $(docker kill c1 r1 r2 s1 s2 bind9)
        docker rmi supblox/dind supblox/vyos
        docker network rm c1-r1 r1-r2 r1-s1 r2-s2
fi